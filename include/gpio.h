/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef OXMOS_GPIO_H
#define OXMOS_GPIO_H

#include <stdint.h>

/* TODO : The GPIO offsets and values to set these offsets to are still
 * mostly magic numbers. */

enum Gpio_type_t {
	GPIO_BASE_ADDR = 0x20200000,
	GPIO_INIT = 1,
	GPIO_ACT_OFF = 6,
	GPIO_ACT_ON = 10,
};

void gpio_init(void);

void gpio_set(enum Gpio_type_t type, uint32_t value);

#endif
