/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef OXMOS_INTERRUPT_H
#define OXMOS_INTERRUPT_H

#include <stdint.h>

enum Irq_list_t {
	IRQ_RESET = 0x0,
	IRQ_UNDEF = 0x1,
	IRQ_SWI   = 0x2,
	IRQ_ABRT  = 0x3,
	IRQ_PGFLT = 0x4,
	IRQ_RESV  = 0x5,
	IRQ_IRQ   = 0x6,
	IRQ_FIQ   = 0x7,
};

enum Int_reg_t {
	INT_BASE_ADDR = 0x2000b000,
	INT_IRQ_BASIC_PENDING = 0x200,
	INT_IRQ_1_PENDING = 0x204,
	INT_IRQ_2_PENDING = 0x208,
	INT_FIQ_CTRL = 0x20c,
	INT_IRQ_1_ENABLE = 0x210,
	INT_IRQ_2_ENABLE = 0x214,
	INT_IRQ_BASIC_ENABLE = 0x218,
	INT_IRQ_1_DISABLE = 0x21c,
	INT_IRQ_2_DISABLE = 0x220,
	INT_IRQ_BASIC_DISABLE = 0x224,
};

enum Int_basic_bits_t {
	ARM_TIMER = 0x1,
	ARM_MAILBOX = 0x2,
	ARM_DOORBELL_0 = 0x4,
	ARM_DOORBELL_1 = 0x8,
	GPU0_HALTED = 0x10,
	GPU1_HALTED = 0x20,
	ILLEGAL_ACCESS_1 = 0x40,
	ILLEGAL_ACCESS_0 = 0x80,
	PENDING_1 = 0x100,
	PENDING_2 = 0x200,
	GPU_7 = 0x400,
	GPU_9 = 0x800,
	GPU_10 = 0x1000,
	GPU_18 = 0x2000,
	GPU_19 = 0x4000,
	GPU_53 = 0x8000,
	GPU_54 = 0x10000,
	GPU_55 = 0x20000,
	GPU_56 = 0x40000,
	GPU_57 = 0x80000,
	GPU_62 = 0x100000,
	
};

void interrupt_install(uint32_t i,
		void (*addr)(uint32_t, uint32_t, uint32_t, uint32_t));

void interrupt_std_enable(void);
void interrupt_fast_enable(void);

void interrupt_basic_enable(uint32_t int_type);
void interrupt_basic_disable(uint32_t int_type);

uint32_t interrupt_get_basic_enabled(void);
uint32_t interrupt_get_basic_pending(void);

void interrupt_1_enable(uint32_t int_type);
void interrupt_1_disable(uint32_t int_type);

uint32_t interrupt_get_1_enabled(void);
uint32_t interrupt_get_1_pending(void);

void __attribute__ ((interrupt("SWI")))
handle_swi(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3);

void __attribute__ ((interrupt("IRQ")))
handle_irq(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3);

void __attribute__ ((interrupt("FIQ")))
handle_fiq(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3);

void __attribute__ ((interrupt("ABORT")))
handle_abort(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3);

void __attribute__ ((interrupt("UNDEF")))
handle_undef(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3);

#endif
