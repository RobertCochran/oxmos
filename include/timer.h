/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef OXMOS_TIMER_H
#define OXMOS_TIMER_H

#include <stdint.h>

enum Timer_type_t {
	TIMER_BASE_ADDR = 0x20003000,
	TIMER_CONTROL = 0,
	TIMER_COUNTER_LOW = 1,
	TIMER_COUNTER_HIGH = 2,
	TIMER_CMP_0 = 3,
	TIMER_CMP_1 = 4,
	TIMER_CMP_2 = 5,
	TIMER_CMP_3 = 6,
};

uint32_t timer_get_control(void);

uint64_t timer_get_counter(void);

void timer_set_cmp(uint8_t ch, uint32_t value);

void timer_clear_match(uint8_t ch);

void sleep(uint32_t microsec);

#endif
