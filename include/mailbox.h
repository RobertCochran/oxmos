/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef OXMOS_MAILBOX_H
#define OXMOS_MAILBOX_H

#include <stdint.h>

enum Mailbox_type_t {
	MAILBOX_BASE_ADDR = 0x2000b880,
	MAILBOX_READ = 0,
	MAILBOX_POLL = 4,
	MAILBOX_SENDERINFO = 5,
	MAILBOX_STATUS = 6,
	MAILBOX_CONFIG = 7,
	MAILBOX_WRITE = 8,
};

void mailbox_write(uint32_t msg, uint8_t dest);

uint32_t mailbox_read(uint8_t src);

#endif
