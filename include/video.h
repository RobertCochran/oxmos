/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef OXMOS_VIDEO_H
#define OXMOS_VIDEO_H

#include <stdint.h>

struct Fb_color_t {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

struct Fb_info_t {
	volatile uint32_t fb_p_width;
	volatile uint32_t fb_p_height;
	volatile uint32_t fb_v_width;
	volatile uint32_t fb_v_height;
	volatile uint32_t fb_gpu_pitch;
	volatile uint32_t fb_depth;
	volatile uint32_t fb_x;
	volatile uint32_t fb_y;
	volatile uint32_t fb_ptr;
	volatile uint32_t fb_size;
} __attribute__ ((aligned (16)));

struct Fb_color_t fb_get_color_24(uint8_t r, uint8_t g, uint8_t b);

uint8_t fb_init(uint32_t w, uint32_t h, uint32_t d);

void fb_set_pixel(uint16_t x, uint16_t y, struct Fb_color_t color);

void fb_clear(struct Fb_color_t color);

void fb_scroll(void);

void fb_putc(const char c);

void fb_putc_raw(const char c);

void fb_puts(const char *s);

void fb_setxy(uint16_t x, uint16_t y);

void fb_setbgc(struct Fb_color_t c);

void fb_setfgc(struct Fb_color_t c);

#endif
