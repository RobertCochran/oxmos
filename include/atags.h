/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef OXMSO_ATAGS_H
#define OXMOS_ATAGS_H

#include <stdint.h>

enum Atag_type_t {
	ATAG_NONE = 0,
	ATAG_CORE = 0x54410001,
	ATAG_MEM = 0x54410002,
	ATAG_VIDEOTEXT = 0x54410003,
	ATAG_RAMDISK = 0x54410004,
	ATAG_INITRD2 = 0x54410005,
	ATAG_SERIAL = 0x54410006,
	ATAG_REVISION = 0x54410007,
	ATAG_VIDEOFLB = 0x54410008,
	ATAG_CMDLINE = 0x54410009,
};

struct Atag_header_t {
        uint32_t size; /* legth of tag in words including this header */
        uint32_t tag;  /* tag value */
};

struct Atag_core_t {
	uint32_t flags; /* bit 0 = read-only */
	uint32_t pagesize; /* systems page size (usually 4k) */
	uint32_t rootdev; /* root device number */
};

struct Atag_mem_t {
	uint32_t size; /* size of the area */
	uint32_t start; /* physical start address */
};

struct Atag_videotext_t {
        uint8_t x; /* width of display */
        uint8_t y; /* height of display */
        uint16_t video_page;
        uint8_t video_mode;
        uint8_t video_cols;
        uint16_t video_ega_bx;
        uint8_t video_lines;
        uint8_t video_isvga;
        uint16_t video_points;
};

struct Atag_ramdisk_t {
        uint32_t flags; /* bit 0 = load, bit 1 = prompt */
        uint32_t size; /* decompressed ramdisk size in _kilo_ bytes */
        uint32_t start; /* starting block of floppy-based RAM disk image */
};

struct Atag_initrd2_t {
        uint32_t start; /* physical start address */
        uint32_t size; /* size of compressed ramdisk image in bytes */
};

struct Atag_serialnr_t {
        uint32_t low;
        uint32_t high;
};

struct Atag_revision_t {
        uint32_t rev;
};

struct Atag_videolfb_t {
        uint16_t lfb_width;
        uint16_t lfb_height;
        uint16_t lfb_depth;
        uint16_t lfb_linelength;
        uint32_t lfb_base;
        uint32_t lfb_size;
        uint8_t red_size;
        uint8_t red_pos;
        uint8_t green_size;
        uint8_t green_pos;
        uint8_t blue_size;
        uint8_t blue_pos;
        uint8_t rsvd_size;
        uint8_t rsvd_pos;
};

struct Atag_cmdline_t {
        char    cmdline[1];
};

struct Atag_t {
        struct Atag_header_t hdr;
        union {
                struct Atag_core_t core;
                struct Atag_mem_t mem;
                struct Atag_videotext_t videotext;
                struct Atag_ramdisk_t ramdisk;
                struct Atag_initrd2_t initrd2;
                struct Atag_serialnr_t serialnr;
                struct Atag_revision_t revision;
                struct Atag_videolfb_t videolfb;
                struct Atag_cmdline_t cmdline;
        } u;
};

#define NEXT_TAG(t) ((struct Atag_t *)((uint32_t *)(t) + (t)->hdr.size))

#endif
