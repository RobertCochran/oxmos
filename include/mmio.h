/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef OXMOS_MMIO_H
#define OXMOS_MMIO_H

#include <stdint.h>

/*uint64_t mmio_read_64(uint32_t addr);
void mmio_write_64(uint32_t addr, uint64_t value);

uint32_t mmio_read_32(uint32_t addr);
void mmio_write_32(uint32_t addr, uint32_t value);

uint16_t mmio_read_16(uint32_t addr);
void mmio_write_16(uint32_t addr, uint16_t value);

uint8_t mmio_read_8(uint32_t addr);
void mmio_write_8(uint32_t addr, uint8_t value);*/

uint32_t mmio_get_cpsr(void);
void mmio_set_cpsr(uint32_t value);

#endif
