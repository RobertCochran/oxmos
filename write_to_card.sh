#!/bin/bash

DEVICE=/dev/mmcblk0p1
LOCATION=~/media/2300-4E18

udisksctl mount -b $DEVICE > /dev/null
rm $LOCATION/kernel.img
cp kernel.img $LOCATION
umount $LOCATION
