/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include "timer.h"

uint32_t *timer_addr = (uint32_t *) TIMER_BASE_ADDR;

uint32_t timer_get_control(void)
{
	return timer_addr[TIMER_CONTROL];
}

uint64_t timer_get_counter(void)
{
	return timer_addr[TIMER_COUNTER_LOW] |
		(timer_addr[TIMER_COUNTER_HIGH] << 31);
}

void timer_set_cmp(uint8_t ch, uint32_t value)
{
	/*
	 * Note : the BCM2835 can't use the timer compare registers
	 * 0 or 2 - those are reserved for use by the VideoCore IV,
	 * so don't bother handling those
	 */

	switch (ch) {
		case 1: timer_addr[TIMER_CMP_1] = value;
			break;
		case 3: timer_addr[TIMER_CMP_3] = value;
			break;
	};
}

void timer_clear_match(uint8_t ch)
{
	timer_addr[TIMER_CONTROL] = timer_addr[TIMER_CONTROL]
		& ~(1 << (ch + 4));
}

void sleep(uint32_t microsec)
{
	uint64_t start_time = timer_get_counter();

	do {
		/* Nothing */
	} while (timer_get_counter() - start_time < microsec);
}
