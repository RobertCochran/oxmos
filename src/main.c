/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

/* main.c - the entry point for the kernel */

#include <stdint.h>

#include "atags.h"
#include "gpio.h"
#include "timer.h"
#include "video.h"
#include "textutil.h"
#include "mmio.h"
#include "interrupt.h"

#define UNUSED(x) (void)(x)

/* kernel main function, it all begins here */
void kernel_main(uint32_t r0, uint32_t mach_type, uint32_t atag_addr)
{
	/* The Linux ATAG docs say it should be 0, so bail if it isn't */
	if (r0 != 0) return;

	/* Initialize GPIO */
	gpio_init();

	/* Turn ACT light on */
	gpio_set(GPIO_ACT_ON, 1 << 16);

	/* Load up framebuffer info and try to initialize */
	uint32_t error = fb_init(1024, 768, 24);

	if (error) {
		/* Blink out an error on the ACT light */
		uint32_t i;

		while (1) {
			for (i = 0; i != 2; i++) {
				gpio_set(GPIO_ACT_ON, 1 << 16);
				sleep(1000000);
				gpio_set(GPIO_ACT_OFF, 1 << 16);
				sleep(1000000);
			}

			for (i = 0; i != error; i++) {
				gpio_set(GPIO_ACT_ON, 1 << 16);
				sleep(500000);
				gpio_set(GPIO_ACT_OFF, 1 << 16);
				sleep(500000);
			}
		}
	}

	/* Yes! A framebuffer! */

	fb_clear(fb_get_color_24(0, 0, 0));

	fb_setbgc(fb_get_color_24(0xff, 0xff, 0xff));
	fb_setfgc(fb_get_color_24(0x0, 0x0, 0xff));

	fb_puts("This is OXMOS, the Open eXtendable and Modifiable "
		"Operating System for the Raspberry Pi microcomputer "
		"system\n");

	fb_setbgc(fb_get_color_24(0, 0, 0));
	fb_setfgc(fb_get_color_24(0x7f, 0x7f, 0x7f));
	fb_puts("Initializing...\n\n");

	fb_puts("Machine type is ");
	fb_puts(tohex(mach_type));

	if (mach_type == 0x0c42)
		fb_puts(" (Raspberry Pi)");
	else
		fb_puts(" (Unknown machine type! Good luck!)");

	fb_puts("\n\n");

	fb_puts("Reading ATAGs...\n\n");

	/* Only do anything with the ATAG_MEM and
 	 * ATAG_CMDLINE types for the moment */
	struct Atag_t *atags;

	uint8_t atag_count = 0;

	for (atags = (struct Atag_t *) atag_addr; atags->hdr.tag != ATAG_NONE;
			atags = NEXT_TAG(atags), atag_count++) {
		if (atags->hdr.tag == ATAG_CORE) {
			fb_puts("ATAG_CORE : ");

			if (atags->hdr.size == 2) {
				fb_puts("No attached info");
			} else {
				fb_puts("\nFlags - ");
				fb_puts(tohex(atags->u.core.flags));
				fb_puts("\n");
				fb_puts("Page Size - ");
				fb_puts(todec(atags->u.core.pagesize));
				fb_puts("\n");
				fb_puts("Root Device # - ");
				fb_puts(todec(atags->u.core.rootdev));
			}
		}

		if (atags->hdr.tag == ATAG_MEM) {
			fb_puts("Memory region at ");
			fb_puts(tohex(atags->u.mem.start));
			fb_puts(" of size ");
			fb_puts(tohex(atags->u.mem.size));
			fb_puts(" (");
			fb_puts(toudec(atags->u.mem.size));
			fb_puts(")");
		}

		if (atags->hdr.tag == ATAG_CMDLINE) {
			fb_puts("Kernel command line - \"");
			fb_puts(atags->u.cmdline.cmdline);
			fb_puts("\"");
		}

		fb_puts("\n\n");
	}

	fb_puts("Processed ");
	fb_puts(todec(atag_count));
	fb_puts(" ATAGs\n");

	/* Install our interrupt handlers */

	interrupt_install(IRQ_SWI, handle_swi);
	//interrupt_install(IRQ_IRQ, handle_irq);

	asm volatile("swi 42");

	/* Enable standard (non-FIQ) interrupt handler */
	//interrupt_std_enable();

	/* Enable interrupts from the ARM timer */
	//interrupt_1_enable(2);

	fb_puts("\nHere's a sample of my character set :\n");

	uint8_t c;

	for (c = 0; c < 128; c++)
		fb_putc_raw(c);

	fb_puts("\n");

	fb_puts("\n*** Stopping kernel_main() code execution ***\n");
}
