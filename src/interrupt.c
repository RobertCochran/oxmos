/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#define UNUSED(x) (void)(x)

#include "interrupt.h"

#include "mmio.h"
#include "gpio.h"
#include "timer.h"
#include "video.h"
#include "textutil.h"

uint32_t *int_addr = (uint32_t *) INT_BASE_ADDR;

void interrupt_install(uint32_t i,
		void (*addr)(uint32_t, uint32_t, uint32_t, uint32_t))
{
	uint32_t *v = (uint32_t *) 0x0;

	uint32_t fixed_addr = (uint32_t) addr;

	/* Subtract the interrupt vector index */
	fixed_addr -= (4 * i);

	/* Subtract 8 to allow for prefetching */
	fixed_addr -= 8;

	/* Shift to give a word offset */
	fixed_addr >>= 2;

	/* Clear the top byte */
	fixed_addr &= 0x00ffffff;

	/* Or with 0xea000000 (branch opcode) to make a proper instruction */
	v[i] = 0xea000000 | (int) fixed_addr;
}

void interrupt_std_enable(void)
{
	mmio_set_cpsr(mmio_get_cpsr() & ~(1 << 7));
}

void interrupt_fast_enable(void)
{
	mmio_set_cpsr(mmio_get_cpsr() & ~(1 << 6));
}

void interrupt_basic_enable(uint32_t type)
{
	int_addr[INT_IRQ_BASIC_ENABLE] = type;
}

void interrupt_basic_disable(uint32_t type)
{
	int_addr[INT_IRQ_BASIC_DISABLE] = type;
}

uint32_t interrupt_get_basic_enabled(void)
{
	return int_addr[INT_IRQ_BASIC_ENABLE];
}

uint32_t interrupt_get_basic_pending(void)
{
	return int_addr[INT_IRQ_BASIC_PENDING];
}

void interrupt_1_enable(uint32_t type)
{
	int_addr[INT_IRQ_1_ENABLE] = type;
}

void interrupt_1_disable(uint32_t type)
{
	int_addr[INT_IRQ_1_DISABLE] = type;
}

uint32_t interrupt_get_1_enabled(void)
{
	return int_addr[INT_IRQ_1_ENABLE];
}

uint32_t interrupt_get_1_pending(void)
{
	return int_addr[INT_IRQ_1_PENDING];
}

void __attribute__ ((interrupt("SWI")))
handle_swi(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3)
{
	UNUSED(r0);
	UNUSED(r1);
	UNUSED(r2);
	UNUSED(r3);

	register uint32_t addr, swi_no;

	asm volatile("mov %[addr], lr" : [addr] "=r" (addr));

	swi_no = *((uint32_t *) addr - 1) & 0x00fffff;

	fb_setfgc(fb_get_color_24(0, 0xff, 0xff));

	fb_puts("\nGot a SWI, code ");
	fb_puts(tohex(swi_no));
	fb_puts("\n");

	fb_setfgc(fb_get_color_24(0x7f, 0x7f, 0x7f));

	return;
}

void __attribute__ ((interrupt("IRQ")))
handle_irq(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3)
{
	UNUSED(r0);
	UNUSED(r1);
	UNUSED(r2);
	UNUSED(r3);

	fb_puts("Got a hardware interrupt of some sort...\n");

	/* Clear interrupt source. Since the timer is the only active source
	 * atm, it's fine just to go ahead and do it */

	timer_clear_match(1);

	int_addr[INT_IRQ_1_PENDING] &= 0x2;
}

void __attribute__ ((interrupt("FIQ")))
handle_fiq(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3)
{
	UNUSED(r0);
	UNUSED(r1);
	UNUSED(r2);
	UNUSED(r3);

	fb_setbgc(fb_get_color_24(0, 0, 0));
	fb_setfgc(fb_get_color_24(0xff, 0, 0));
	fb_puts("\nAn unhandled FIQ interrupt has occured!!\n");
	/* Hang */
	while (1);
}


void __attribute__ ((interrupt("ABORT")))
handle_abort(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3)
{
	UNUSED(r0);
	UNUSED(r1);
	UNUSED(r2);
	UNUSED(r3);

	fb_setbgc(fb_get_color_24(0, 0, 0));
	fb_setfgc(fb_get_color_24(0xff, 0, 0));
	fb_puts("\nAn unhandled page fault has occured!!\n");
	/* Hang */
	while (1);
}

void __attribute__ ((interrupt("UNDEF")))
handle_undef(uint32_t r0, uint32_t r1, uint32_t r2, uint32_t r3)
{
	UNUSED(r0);
	UNUSED(r1);
	UNUSED(r2);
	UNUSED(r3);

	fb_setbgc(fb_get_color_24(0, 0, 0));
	fb_setfgc(fb_get_color_24(0xff, 0, 0));
	fb_puts("\nAn unhandled UNDEF interrupt has occured!!\n");
	/* Hang */
	while (1);
}
