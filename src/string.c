/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include "string.h"

void *memcpy(void *dest, void *src, uint32_t n)
{
	uint32_t i;

	for (i = 0; i < n / 4; i++)
		*((uint32_t *) dest + i) = *((uint32_t *) src + i);

	if (n % 4 == 0) return dest;

	i *= 4; /* Scale to 8 bit ints */

	for (; i < n; i++)
		*((uint8_t *) dest + i) = *((uint8_t *) src + i);

	return dest;
}
