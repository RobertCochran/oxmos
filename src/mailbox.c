/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include "mailbox.h"
#include "timer.h"

uint32_t *mailbox_addr = (uint32_t *) MAILBOX_BASE_ADDR;

void mailbox_write(uint32_t msg, uint8_t dest)
{
	if (dest > 7)
		return;

	volatile uint32_t full_msg = msg;
	/* Set low four bits to 0 */
	full_msg = msg & (~0xf);

	/* Add channel, ignoring top 4 bits */
	full_msg = full_msg + (dest & 0xf);

	/* Wait until ready */
	while (mailbox_addr[MAILBOX_STATUS] & 1 << 31)
		sleep(10);

	mailbox_addr[MAILBOX_WRITE] = full_msg;
}

uint32_t mailbox_read(uint8_t src)
{
	while (1) {
		/* Wait until ready */
		while (mailbox_addr[MAILBOX_STATUS] & 1 << 31)
			sleep(10);

		volatile uint32_t data = mailbox_addr[MAILBOX_READ];

		if ((data & 0xf) == src)
			return (data >> 4);
	}
}
