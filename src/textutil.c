/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

/* Convert an unsigned value to hex (with the trailing "0x")
 * size = size in bytes (only 1, 2 or 4 permitted)
 */

#include "textutil.h"

char *toudec(uint32_t n)
{
	static char s[11];

	uint8_t i;

	for (i = 10; n != 0 || i == 10; i--, n = n / 10)
		s[i] = 48 + (n % 10);

	return &s[i + 1];
}

char *todec(int32_t n)
{
	static char s[12];

	int32_t work_n = n;

	if (work_n < 0)
		work_n = -work_n;

	uint8_t i;

	for (i = 11; work_n != 0 || i == 11; i--, work_n = work_n / 10)
		s[i] = 48 + (work_n % 10);

	if (n < 0) {
		s[i] = '-';
		i -= 1;
	}

	return &s[i + 1];
}

char *tohex(uint32_t n)
{
	static char s[11];

	uint8_t size;

	if (n < 2 << 4)
		size = 1;
	else if (n < 2 << 8)
		size = 2;
	else if (n < 2 << 16)
		size = 4;
	else
		size = 8;

	uint8_t i;

	for (i = 10; size != 0 || i == 10; i--, size--, n = n >> 4) {
		s[i] = 48 + (n & 15);

		if (s[i] >= 58) s[i] += 39;
	}

        s[i] = 'x';
	i -= 1;
	s[i] = '0';

	return &s[i];
}
