/*
 * OXMOS - The Open eXtendable and Modifiable Operating System
 * for the Raspberry Pi microcomputer
 *
 * Copyright (C) 2014 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include "video.h"
#include "mailbox.h"
#include "string.h"

#include "font.h"

struct Fb_info_t fb_info;
uint16_t char_x, char_y;
struct Fb_color_t char_bgc, char_fgc;

struct Fb_color_t fb_get_color_24(uint8_t r, uint8_t g, uint8_t b)
{
	return (struct Fb_color_t) {
		.r = r,
		.g = g,
		.b = b,
	};
}

uint8_t fb_init(uint32_t w, uint32_t h, uint32_t d)
{
	fb_info.fb_p_width = fb_info.fb_v_width = w;
	fb_info.fb_p_height = fb_info.fb_v_height = h;

	fb_info.fb_depth = d;

	fb_info.fb_x = fb_info.fb_y = 0;

	mailbox_write((uint32_t) &fb_info + 0x40000000, 1);

	return mailbox_read(1);
}

void fb_set_pixel(uint16_t x, uint16_t y, struct Fb_color_t color)
{
	if (x > fb_info.fb_v_width || y > fb_info.fb_v_height) return;

	uint32_t offset = (x + (y * fb_info.fb_v_width))
		* (fb_info.fb_depth / 8);

	*((uint8_t*) fb_info.fb_ptr + offset) = color.r;
	*((uint8_t*) fb_info.fb_ptr + offset + 1) = color.g;
	*((uint8_t*) fb_info.fb_ptr + offset + 2) = color.b;
}

void fb_clear(struct Fb_color_t color)
{
	uint16_t x, y;

	for (y = 0; y != fb_info.fb_v_height; y++)
		for (x = 0; x != fb_info.fb_v_width; x++)
			fb_set_pixel(x, y, color);
}

void fb_scroll(void)
{
	memcpy((void *) fb_info.fb_ptr,
	       (void *) (fb_info.fb_ptr +
	       ((fb_info.fb_depth / 8) * (fb_info.fb_v_width * 16))),
	       (fb_info.fb_v_height - 16) * fb_info.fb_v_width
	       * (fb_info.fb_depth / 8));
}

void fb_putc(const char c)
{
	if (c > 127) return;

	/* Handle newlines */
	if (c == '\n') {
		char_x = 0; char_y++;
		return;
	}

	/* Handle carriage returns */
	if (c == '\r') {
		char_x = 0;
		return;
	}

	if (char_x >= (fb_info.fb_v_width / 8)) {
		char_x = 0; char_y++;
	}

	if (char_y >= (fb_info.fb_v_height / 16)) {
		char_y = (fb_info.fb_v_height / 16) - 1;
		char_x = 0;
		fb_scroll();
	}

	/* c * 16 */
	uint16_t ch_addr = c << 4;

	uint16_t bit, row;

	for (row = 0; row != 16; row++)
		for (bit = 0; bit != 8; bit++)
			if (font[ch_addr + row] & (1 << bit))
				fb_set_pixel((char_x * 8) + bit,
					(char_y * 16) + row, char_fgc);
			else
				fb_set_pixel((char_x * 8) + bit,
					(char_y * 16) + row, char_bgc);

	char_x++;
}

void fb_putc_raw(const char c)
{
	if (c > 127) return;

	if (char_x >= (fb_info.fb_v_width / 8)) {
		char_x = 0; char_y++;
	}

	if (char_y >= (fb_info.fb_v_height / 16))
		char_y = 0;

	/* c * 16 */
	uint16_t ch_addr = c << 4;

	uint16_t bit, row;

	for (row = 0; row != 16; row++)
		for (bit = 0; bit != 8; bit++)
			if (font[ch_addr + row] & (1 << bit))
				fb_set_pixel((char_x * 8) + bit,
					(char_y * 16) + row, char_fgc);
			else
				fb_set_pixel((char_x * 8) + bit,
					(char_y * 16) + row, char_bgc);

	char_x++;
}

void fb_puts(const char *s)
{
	for (; *s != '\0'; s++)
		fb_putc(*s);
}

void fb_setxy(uint16_t x, uint16_t y)
{
	char_x = x; char_y = y;
}

void fb_setbgc(struct Fb_color_t c)
{
	char_bgc = c;
}

void fb_setfgc(struct Fb_color_t c)
{
	char_fgc = c;
}
